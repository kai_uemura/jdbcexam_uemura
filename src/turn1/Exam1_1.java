package turn1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Exam1_1 {

	public static void main(String[] args) {
		final String url = "jdbc:mysql://localhost/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		Connection conn = null;
		/**
		 * 接続のテストのみ行う
		 */
		try {
			conn = DriverManager.getConnection(url, username, password);
			//1.2.10 Statementオブジェクトの生成の部分
			Statement stmt = conn.createStatement();
			String query = "select\n" +
					"i.item_id,i.item_name,i.image_url,i.item_size,i.price,\n" +
					"s.quantity,i.is_delete from items i inner join stocks s\n" +
					"on i.item_id = s.item_id;";
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				int id = rs.getInt(1); //item_id
				String item_name = rs.getString("item_name");
				String image_url = rs.getString("image_url");
				String item_size = rs.getString("item_size");
				int price = rs.getInt("price");
				int quantity = rs.getInt("quantity");
				int is_delete = rs.getInt("is_delete");
				System.out.printf("%d : %s : %s : %s : %d : %d : %d %n",id, item_name, image_url, item_size, price, quantity, is_delete);
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			//以下教科書では1.2.15 接続の解除(2)に掲載されている部分
		}finally{
			if(conn!=null){
				try {
					//closeも
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
