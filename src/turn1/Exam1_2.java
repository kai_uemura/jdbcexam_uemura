package turn1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Exam1_2 {

	public static void main(String[] args) {
		final String url = "jdbc:mysql://localhost/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		Connection conn = null;
		/**
		 * 接続のテストのみ行う
		 */
		try {
			conn = DriverManager.getConnection(url, username, password);
			//1.2.10 Statementオブジェクトの生成の部分
			Statement stmt = conn.createStatement();
			String query = "INSERT INTO items VALUES ('00000006','デッキチェア\n" +
					"','/images/eyecatch5.jpg','100x60x70',99999,false);";
			int result1 = stmt.executeUpdate(query);
			if(result1!=1){
				System.err.println("挿⼊の失敗");
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			//以下教科書では1.2.15 接続の解除(2)に掲載されている部分
		}finally{
			if(conn!=null){
				try {
					//closeも
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
