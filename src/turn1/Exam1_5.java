package turn1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Exam1_5 {

	public static void main(String[] args) {
		final String url = "jdbc:mysql://localhost/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		Connection conn = null;

		final String sql = "insert into items values"
				+ "(?, ?, ?, ? , ? , ?)";
		try {
			conn = DriverManager.getConnection(url, username, password);
			//1.2.10 Statementオブジェクトの生成の部分
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, "00000007");
			stmt.setString(2, "Exam1_5追加商品");
			stmt.setString(3, "/images/eyecatch5.jpg");
			stmt.setString(4, "100x60x70");
			stmt.setInt(5, 100);
			stmt.setBoolean(6, false);
			int result1 = stmt.executeUpdate();
			if(result1!=1){
				System.err.println("挿⼊の失敗");
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			//以下教科書では1.2.15 接続の解除(2)に掲載されている部分
		}finally{
			if(conn!=null){
				try {
					//closeも
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
