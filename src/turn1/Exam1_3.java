package turn1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Exam1_3 {

	public static void main(String[] args) {
		final String url = "jdbc:mysql://localhost/yasui?serverTimezone=JST&characterEncoding=UTF-8";
		final String username = "yasui";
		final String password = "password";
		Connection conn = null;
		/**
		 * 接続のテストのみ行う
		 */
		try {
			conn = DriverManager.getConnection(url, username, password);
			//1.2.10 Statementオブジェクトの生成の部分
			Statement stmt = conn.createStatement();
			String query = "update stocks set quantity = 50 where item_id =\n" +
					"'00000005';";
			int result1 = stmt.executeUpdate(query);
			if(result1!=1){
				System.err.println("更新の失敗");
			}
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			e.printStackTrace();
			//以下教科書では1.2.15 接続の解除(2)に掲載されている部分
		}finally{
			if(conn!=null){
				try {
					//closeも
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
